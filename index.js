"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
// Keep, to use pjson.personalizedServerURL as URL for your personal server.
const pjson = require('../../package.json');
function myFunction(http) {
    return new Promise((resolve, reject) => {
        http.setDataSerializer('json');
        http.setServerTrustMode('nocheck').then((res) => {
            // Adjust the route to the one you have chosen for your server route.
            // Replace my_response_param by the parameter you need for your server, including the value.
            http.post(pjson.personalizedServerURL + '/my_server_route', {
                my_response_param: ""
            }, {}).then((data) => {
                const response = data.data;
                // return the result from the Personal Server to your application.
                resolve(response);
            }).catch((error) => {
                // Error occured, your application will get an exception.
                reject(error);
            });
        }).catch((sslError) => {
            reject(sslError);
        });
    });
}
exports.myFunction = myFunction;
