module.exports = {
	startListening: function() {
		cordova.exec(successCallback, errorCallback, 'COMPRISE_SpeechToText', 'startListening', []);
	},
	stopListening: function(successCallback, errorCallback) {
		cordova.exec(successCallback, errorCallback, 'COMPRISE_SpeechToText', 'stopListening', []);
	},
	hasPermission: function(successCallback, errorCallback) {
		cordova.exec(successCallback, errorCallback, 'COMPRISE_SpeechToText', 'hasPermission', []);
	},
	requestPermission: function(successCallback, errorCallback) {
		cordova.exec(successCallback, errorCallback, 'COMPRISE_SpeechToText', 'requestPermission', []);
	},
	initialize: function(successCallback, errorCallback) {
		cordova.exec(successCallback, errorCallback, 'COMPRISE_SpeechToText', 'initialize', []);
	},
	recognizeFile: function(base64audio, successCallback, errorCallback) {
		 cordova.exec(successCallback, errorCallback, 'COMPRISE_SpeechToText', 'recognizeFile', [base64audio]);
	}
};