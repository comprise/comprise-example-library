package eu.comprise;

import org.apache.cordova.CallbackContext;
import org.apache.cordova.CordovaInterface;
import org.apache.cordova.CordovaPlugin;
import org.apache.cordova.CordovaWebView;
import org.apache.cordova.PluginResult;

import org.json.JSONArray;
import org.json.JSONException;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;

import android.Manifest;
import android.media.MediaPlayer;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;

import android.util.Log;
import android.view.View;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import org.json.JSONObject;
import org.kaldi.Assets;
import org.kaldi.KaldiRecognizer;
import org.kaldi.Model;
import org.kaldi.RecognitionListener;
import org.kaldi.SpeechRecognizer;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.lang.ref.WeakReference;

import eu.comprise.sampleapp.R;


public class COMPRISE_SpeechToText extends CordovaPlugin implements RecognitionListener {

  static {
    System.loadLibrary("kaldi_jni");
  }

  private static final String LOG_TAG = "COMPRISE_SpeechToText";

  static private final int STATE_START = 0;
  static private final int STATE_READY = 1;
  static private final int STATE_MIC  = 2;

  /* Used to handle permission request */
  private static final int PERMISSIONS_REQUEST_RECORD_AUDIO = 1;

  private Model model;
  private SpeechRecognizer recognizer;

  private static final String START_LISTENING = "startListening";
  private static final String STOP_LISTENING = "stopListening";
  private static final String HAS_PERMISSION = "hasPermission";
  private static final String REQUEST_PERMISSION = "requestPermission";
  private static final String MISSING_PERMISSION = "Missing permission";
  private static final String INITIALIZE = "initialize";

  private static final String RECORD_AUDIO_PERMISSION = Manifest.permission.RECORD_AUDIO;
  private static final String WRITE_STORAGE_PERMISSION = Manifest.permission.WRITE_EXTERNAL_STORAGE;
  private static final String[] PERMISSIONS = {RECORD_AUDIO_PERMISSION,WRITE_STORAGE_PERMISSION};

  private CallbackContext callbackContext;
  private Activity compriseActivity;
  private Context context;
  private View view;
  private PluginResult pluginResult;
  @Override
  public void initialize(CordovaInterface cordova, CordovaWebView webView) {
    super.initialize(cordova, webView);

    compriseActivity = cordova.getActivity();
    context = webView.getContext();
    view = webView.getView();
  }

  @Override
  public void onPartialResult(String s) {

  }

  @Override
  public void onResult(String jsonstring) {
    Log.d(LOG_TAG, "SpeechRecognitionListener results: " + jsonstring);

    try {
      JSONObject result = new JSONObject(jsonstring);
      String text = result.getString("text");

      if(!text.isEmpty()) {
        
        MediaPlayer mp = MediaPlayer.create(compriseActivity, R.raw.record_end);
        mp.start();                                                     
        
        pluginResult = new PluginResult(PluginResult.Status.OK, result.getString("text"));
        callbackContext.sendPluginResult(pluginResult);

        if(recognizer != null) {
          recognizer.cancel();
          recognizer = null;
        }
      }
    } catch (Exception e) {
      e.printStackTrace();
      pluginResult = new PluginResult(PluginResult.Status.ERROR, e.getMessage());
      callbackContext.sendPluginResult(pluginResult);
    }
  }

  @Override
  public void onError(Exception e) {
    Log.e(LOG_TAG, e.getMessage());
  }

  @Override
  public void onTimeout() {
    recognizer.cancel();
    recognizer = null;
  }
  
  @Override
  public boolean execute(String action, JSONArray args, CallbackContext callbackContext) throws JSONException {
    this.callbackContext = callbackContext;

    Log.d(LOG_TAG, "execute() action " + action);

    try {

      if (START_LISTENING.equals(action)) {

        if (!permissionsGranted()) {
          callbackContext.error(MISSING_PERMISSION);
          return true;
        }

        startListening();

        return true;
      }

      if (STOP_LISTENING.equals(action)) {
        final CallbackContext callbackContextStop = this.callbackContext;
        view.post(new Runnable() {
          @Override
          public void run() {
            if(recognizer != null) {
              recognizer.cancel();
              recognizer = null;
            }
            pluginResult = new PluginResult(PluginResult.Status.OK, "Stop");
            callbackContext.sendPluginResult(pluginResult);
            callbackContextStop.success();
          }
        });
        return true;
      }

      if (HAS_PERMISSION.equals(action)) {
        hasPermissions();
      }

      if (REQUEST_PERMISSION.equals(action)) {
        requestPermissions();
        return true;
      }

      if (INITIALIZE.equals(action)) {
        view.post(new Runnable() {
          @Override
          public void run() {
            if (!permissionsGranted()) {
              callbackContext.error(MISSING_PERMISSION);
            }
            new SetupTask(COMPRISE_SpeechToText.this).execute();

            pluginResult = new  PluginResult(PluginResult.Status.NO_RESULT);
            pluginResult.setKeepCallback(true); // Keep callback
          }
        });
        return true;
      }




    } catch (Exception e) {
      e.printStackTrace();
      callbackContext.error(e.getMessage());
    }

    return false;
  }

  private void startListening() {
    Log.d(LOG_TAG, "startListening()");

    if (recognizer != null) {
      recognizer.cancel();
      recognizer = null;
    } else {

      try {
        recognizer = new SpeechRecognizer(model);
        recognizer.addListener(this);
        recognizer.startListening();

        MediaPlayer mp = MediaPlayer.create(compriseActivity, R.raw.record);
        mp.start();
      } catch (IOException e) {
        Log.e(LOG_TAG, e.getMessage());
      }
    }
  }

  private void hasPermissions() {
    PluginResult result = new PluginResult(PluginResult.Status.OK, permissionsGranted());
    result.setKeepCallback(true);
    this.callbackContext.sendPluginResult(result);
  }

  private boolean permissionsGranted() {
    if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M) {
      return true;
    }
    boolean hasPermissions = true;
    for(int i = 0; i < PERMISSIONS.length; i++) {
      if(!cordova.hasPermission(PERMISSIONS[i])) hasPermissions = false;
    }
    return hasPermissions;
  }

  private void requestPermissions() {
    if (!permissionsGranted()) {
      cordova.requestPermissions(this, 23456, PERMISSIONS);
    } else {
      this.callbackContext.success();
    }
  }

  @Override
  public void onRequestPermissionResult(int requestCode, String[] permissions, int[] grantResults) throws JSONException {
    if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
      this.callbackContext.success();
    } else {
      this.callbackContext.error("Permission denied");
    }
  }

  private static class SetupTask extends AsyncTask<Void, Void, Exception> {
    WeakReference<COMPRISE_SpeechToText> activityReference;

    SetupTask(COMPRISE_SpeechToText activity) {
      this.activityReference = new WeakReference<>(activity);
    }

    @Override
    protected Exception doInBackground(Void... params) {
      try {
        Assets assets = new Assets(this.activityReference.get().compriseActivity);
        File assetDir = assets.syncAssets();
        Log.d(LOG_TAG, assetDir.toString());
        activityReference.get().model = new Model(assetDir.toString() + "/model-android");
      } catch (IOException e) {
        return e;
      }
      return null;
    }

    @Override
    protected void onPostExecute(Exception e) {
      if (e != null) {
        Log.e(LOG_TAG, e.getMessage());
        this.activityReference.get().callbackContext.error(e.getMessage());
        this.activityReference.get().pluginResult = new PluginResult(PluginResult.Status.ERROR, e.getMessage());
        this.activityReference.get().callbackContext.sendPluginResult(this.activityReference.get().pluginResult);
      } else {
        this.activityReference.get().pluginResult = new PluginResult(PluginResult.Status.OK, "Is ready");
        this.activityReference.get().callbackContext.sendPluginResult(this.activityReference.get().pluginResult);
        Log.d(LOG_TAG, "Is ready");
      }
    }
  }
}


