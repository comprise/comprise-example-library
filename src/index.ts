// Keep, to use pjson.personalizedServerURL as URL for your personal server.
const pjson = require('../../package.json');

export function myFunction(http: any) {
	return new Promise<any>((resolve, reject) => {
		http.setDataSerializer('json');
			http.setServerTrustMode('nocheck').then((res: any) => {
				// Adjust the route to the one you have chosen for your server route.
				// Replace my_response_param by the parameter you need for your server, including the value.
				http.post(pjson.personalizedServerURL+'/my_server_route', {
					my_response_param: "" 
				}, {}).then((data: any) => {
					const response = data.data
					// return the result from the Personal Server to your application.
					resolve(response);
				}).catch((error: any) => {
					// Error occured, your application will get an exception.
					reject(error);
				});
			}).catch((sslError: any) => {
				reject(sslError);
			});
	});
}
 